from pt import TrieBase
from pt import Node as NodeBase
from util import *
import math


class Node(NodeBase):
    def __init__(self, b=None, p_=None, key1=None, key2=None, r=None):
        super().__init__(b, p_, key1)
        self.key2 = key2
        self.r = r

    def is_msd(self):
        return self.key1 is None and self.key2 is None

    def graph_node(self):
        n = super().graph_node()
        if self.is_msd():
            n.set_color('gray')
        #if self.key2 is not None:
        #    n.set_name(n.get_name() + "\n" + key_to_str(self.key2))
        return n


class HashedPatriciaTrie(TrieBase):
    def search(self, x):
        v = self._read(x)
        if v is not None and (v.key1 == x or v.key2 == x):
            return v

        s = math.floor(math.log2(len(x)))
        k = 0
        v = self._read(())
        p = v.p[x[0]]

        while s >= 0:
            v_ = self._read(x[:k + 2**s])
            if v_ is not None:
                v = v_
                k += 2**s
                if k >= len(x) or v.p[x[k]] is None:
                    break
                p = v.b + v.p[x[k]]
            elif is_prefix(x[:k + 2**s], p):
                k += 2**s
            s -= 1

        while is_prefix(v.b, x) and len(v.b) < len(x) or v.is_msd():
            xk = x[len(v.b)] if len(v.b) < len(x) else 0
            if v.p[xk] is not None:
                v = self._read(v.b + v.p[xk])
            elif v.p[1 - xk] is not None:
                v = self._read(v.b + v.p[1 - xk])
            else:
                break

        return v

    def prefix_search(self, x):
        v = self.search(x)
        if v.key1 is not None:
            return v.key1
        return v.key2

    def msd_node(self, v, w):
        m = msd(len(v.b), len(w.b))
        l = len(w.b) & (~0 << m)
        if l == len(w.b) or l == len(v.b):
            return None
        u = Node(b=w.b[:l], p_=len(v.b))
        v.set_child(u)
        u.set_child(w)
        w.p_ = l
        self._write(u.b, u)
        return u

    def insert(self, x):
        if len(self.hash_table) == 0:
            w = Node(b=(), key2=x)
            w.p[x[0]] = x
            v = Node(b=x, key1=x, r=w.b, p_=0)
            self.msd_node(w, v)
            self._write(w.b, w)
            self._write(v.b, v)
        else:
            v = self.search(x)
            if v.key1 != x and v.key2 != x:
                u = self._read(v.b[:v.p_])
                m = None
                if u.is_msd():
                    m = u
                    u = self._read(u.b[:u.p_])
                if is_prefix(x, v.b):
                    if x == u.b:
                        u.key1 = x
                    elif x == v.b:
                        v.key1 = x
                    else:  # len(u.b) < len(x) < len(v.b)
                        if m is not None:  # not clear in paper: we need to update this node anyway, right?
                            self._delete(m.b)
                        w = Node(b=x, key1=x)
                        u.set_child(w)
                        w.set_child(v)
                        self.msd_node(u, w)
                        self.msd_node(w, v)
                        self._write(w.b, w)
                elif is_prefix(v.b, x):
                    w = Node(b=x, key1=x)
                    if v.p == (None, None) and len(v.b) > 0:
                        self._read(v.r).key2 = x
                        w.r = v.r
                        v.r = None
                    elif v.key2 is None:
                        v.key2 = x
                        w.r = v.b
                    v.set_child(w)
                    self.msd_node(v, w)
                    self._write(w.b, w)
                else:
                    if u.p[x[len(u.b)]] is not None:
                        if m is not None:
                            self._delete(m.b)
                        j = 0
                        while v.b[j] == x[j]:
                            j += 1
                        w_ = Node(b=x[:j], key2=x)
                        w = Node(b=x, key1=x, r=w_.b)
                        u.set_child(w_)
                        w_.set_child(w)
                        w_.set_child(v)
                        self._write(w.b, w)
                        self._write(w_.b, w_)
                        self.msd_node(u, w_)
                        self.msd_node(w_, v)
                        self.msd_node(w_, w)
                    else:
                        w = Node(b=x, key1=x)
                        self._write(w.b, w)
                        u.set_child(w)
                        if u.key2 is None:
                            u.key2 = x
                            w.r = u.b

    def delete(self, x):
        v = self._read(x)
        w = None if v.r is None else self._read(v.r)
        u = self._read(v.b[:v.p_])
        m = None
        if u.is_msd():
            m = u
            u = self._read(u.b[:u.p_])
        if v.key2 is not None:
            v.key1 = None
        else:
            pw_ = v.p[0] or v.p[1]
            if pw_ is not None:
                w_ = self._read(pw_)
                m_ = None
                if w_.is_msd():
                    m_ = w_
                    w_ = self._read(w_.b + (w_.p[0] or w_.p[1]))
                u.set_child(w_)
                self.msd_node(u, w_)
                self._delete(v.b)
                if m is not None:
                    self._delete(m.b)
                if m_ is not None:
                    self._delete(m_.b)
            else:
                u.p[x[len(u.b)]] = None
                if w is None:
                    if u.key2 is not None:
                        self._read(u.key2).r = None
                        u.key2 = None
                elif u.key2 is None:
                    u.r = v.r
                    w.key2 = u.key1
                else:
                    w.key2 = u.key2
                    self._read(u.key2).r = w.b
                    u.key2 = None
                self._delete(v.b)
                if m is not None:
                    self._delete(m.b)

                if u.key1 is None and len(u.b) > 0:
                    p = self._read(u.b[:u.p_])
                    if p.is_msd():
                        self._delete(p.b)
                        p = self._read(p.b[:p.p_])
                    self._delete(u.b)
                    c = self._read(u.b + (u.p[0] or u.p[1]))
                    if c.is_msd():
                        self._delete(c.b)
                        c = self._read(c.b + (c.p[0] or c.p[1]))
                    p.set_child(c)
                    self.msd_node(p, c)
