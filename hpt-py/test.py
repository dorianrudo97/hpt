import unittest
import random

from hpt import HashedPatriciaTrie
from pt import PatriciaTrie


def generate_keys(min_len, max_len, count, seed):
    random.seed(seed)
    for _ in range(count):
        rand_len = random.randint(min_len, max_len)
        num = random.randint(0, 2**rand_len - 1)
        yield tuple(1 if num & (1 << i) else 0 for i in range(rand_len - 1, -1, -1))


class TrieTester:
    def __init__(self, trie, keys, test):
        self.trie = trie
        self.keys = keys
        self.test = test
        for k in keys:
            trie.insert(k)
        self.traverse()

    def insert(self, x):
        self.keys.add(x)
        self.trie.insert(x)
        self.traverse()

    def delete(self, x):
        self.keys.remove(x)
        self.trie.delete(x)
        self.traverse()

    def _prefix_search(self, x):
        lcp = None
        l = -1
        for k in self.keys:
            i = 0
            while i < min(len(x), len(k)) and x[i] == k[i]:
                i += 1
            if i > l:
                l = i
                lcp = [k]
            elif i == l:
                lcp.append(k)
        return lcp

    def prefix_search(self, x):
        prefixes = self._prefix_search(x)
        prefix = self.trie.prefix_search(x)
        self.test.assertIn(prefix, prefixes)
        return prefix

    def traverse(self):
        keys1 = sorted(self.keys)
        keys2 = list(self.trie.traverse())
        self.test.assertEqual(keys1, keys2)
        return keys1


class PtTest(unittest.TestCase):
    TrieType = PatriciaTrie

    def test_insert_20_length_6(self):
        keys = set(generate_keys(6, 6, 20, 0))
        tester = TrieTester(self.TrieType(), keys, self)
        for key in keys:
            tester.prefix_search(key)
        for key in generate_keys(1, 10, 4, 50):
            tester.prefix_search(key)

    def test_insert_1000_length_32(self):
        keys = set(generate_keys(32, 32, 1000, 0))
        tester = TrieTester(self.TrieType(), keys, self)
        for key in keys:
            tester.prefix_search(key)
        for key in generate_keys(1, 40, 1000, 0):
            tester.prefix_search(key)

    def test_insert_100_length_32_to_64(self):
        keys = set(generate_keys(32, 64, 100, 0))
        tester = TrieTester(self.TrieType(), keys, self)
        for key in generate_keys(1, 100, 200, 0):
            tester.prefix_search(key)

    def test_insert_100_length_32_to_64_delete_50(self):
        keys = list(generate_keys(32, 64, 100, 0))
        tester = TrieTester(self.TrieType(), set(keys), self)
        for key in set(keys[:50]):
            tester.delete(key)
        for key in generate_keys(1, 100, 200, 0):
            tester.prefix_search(key)


class HptTest(PtTest):
    TrieType = HashedPatriciaTrie
