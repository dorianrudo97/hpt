from util import *


class TrieBase:
    def __init__(self):
        self.hash_table = {}
        self.reads = 0
        self.writes = 0

    def _read(self, key):
        self.reads += 1
        if key in self.hash_table:
            return self.hash_table[key]
        return None

    def _write(self, key, data):
        self.writes += 1
        self.hash_table[key] = data

    def _delete(self, key):
        self.writes += 1
        del self.hash_table[key]

    def traverse(self):
        v = self._read(())
        yield from self._traverse(v)

    def _traverse(self, v):
        if v.key1 is not None:
            yield v.key1
        if v.p[0] is not None:
            yield from self._traverse(self._read(v.b + v.p[0]))
        if v.p[1] is not None:
            yield from self._traverse(self._read(v.b + v.p[1]))
        yield from ()

    def render(self, filename):
        import pydot
        graph = pydot.Dot(graph_type='graph')
        root = pydot.Node()
        self._render(self._read(()), graph, root)
        graph.write_png(filename)

    def _render(self, node, graph, graph_node):
        import pydot
        graph.add_node(graph_node)
        for p in node.p:
            if p is not None:
                child = self._read(node.b + p)
                child_node = child.graph_node()
                edge = pydot.Edge(graph_node, child_node, label=key_to_str(p))
                graph.add_edge(edge)
                self._render(child, graph, child_node)


class Node:
    def __init__(self, b=None, p_=None, key1=None):
        self.p = [None, None]
        self.b = b
        self.p_ = p_
        self.key1 = key1

    def set_child(self, n):
        self.p[n.b[len(self.b)]] = n.b[len(self.b):]
        n.p_ = len(self.b)

    def graph_node(self):
        import pydot
        n = pydot.Node(name=key_to_str(self.b))
        if self.key1:
            n.set_shape('box')
        return n

    def __str__(self):
        return key_to_str(self.b)


class PatriciaTrie(TrieBase):
    def prefix_search(self, x):
        v = self._read(())
        while len(v.b) < len(x) and is_prefix(v.b, x) and v.p[x[len(v.b)]] is not None:
            v = self._read(v.b + v.p[x[len(v.b)]])
        while v.key1 is None:
            v = self._read(v.b + (v.p[0] or v.p[1]))
        return v.key1

    def insert(self, x):
        w = Node(b=x, key1=x)
        if len(self.hash_table) == 0:
            v = Node(b=())
            v.set_child(w)
            self._write(w.b, w)
            self._write(v.b, v)
            return
        v = self._read(())
        self._write(x, w)
        while is_prefix(v.b, x) and v.p[x[len(v.b)]] is not None:
            v = self._read(v.b + v.p[x[len(v.b)]])
        if is_prefix(v.b, x):
            if v.b != x:
                v.set_child(w)
        else:
            i = 0
            while x[i] == v.b[i]:
                i += 1
            w_ = Node(b=x[:i])
            u = self._read(v.b[:v.p_])
            u.set_child(w_)
            w_.set_child(w)
            w_.set_child(v)
            self._write(w_.b, w_)

    def delete(self, x):
        v = self._read(x)
        if all(v.p):  # v has two children and therefore cannot be deleted
            v.key1 = None
        else:  # v will be deleted
            u = self._read(v.b[:v.p_])  # parent of v
            if any(v.p):  # v has one child which will be attached to u
                u.set_child(self._read(v.b + (v.p[0] or v.p[1])))
            elif len(u.b) == 0:  # v is a leaf, u is root and therefore cannot be deleted
                u.p[x[len(u.b)]] = None
            else:  # v is leaf, u can be deleted
                w = self._read(u.b + u.p[1 - x[len(u.b)]])  # other child of u
                q = self._read(u.b[:u.p_])  # parent of u
                q.set_child(w)
                self._delete(u.b)
            self._delete(x)