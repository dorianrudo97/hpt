from unittest import TestCase

from hpt import HashedPatriciaTrie
from test import TrieTester, generate_keys


def run():
    trie = HashedPatriciaTrie()
    keys = list(generate_keys(4, 6, 20, 0))
    tester = TrieTester(trie, set(keys), TestCase())
    tester.delete(keys[0])
    trie.render('trie.png')


if __name__ == '__main__':
    run()
