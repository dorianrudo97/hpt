def msd(a, b):
    return (a ^ b).bit_length() - 1


def is_prefix(a, b):
    return a is not None and b is not None and len(a) <= len(b) and a == b[:len(a)]


def key_to_str(key):
    return "".join(str(x) for x in key)