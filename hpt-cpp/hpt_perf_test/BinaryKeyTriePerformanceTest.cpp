#include "BinaryKeyTriePerformanceTest.h"

#include <chrono>
#include <iostream>
#include <vector>
#include <iomanip>
#include <PatriciaTrie.h>
#include <HashedPatriciaTrie.h>

void BinaryKeyTriePerformanceTest::run(int iterations) {
    std::cout << "Start: " << name << std::endl;
    init();
    run_test<PatriciaTrie>("Patricia trie", iterations);
    run_test<HashedPatriciaTrie>("Hashed patricia trie", iterations);
}

BinaryKeyTriePerformanceTest::BinaryKeyTriePerformanceTest(std::string name, bool new_trie)
        : name(name), new_trie(new_trie) { }

template<class Trie>
void BinaryKeyTriePerformanceTest::run_test(const std::string& trie_type, int iterations) {
    std::cout << "Trie type: " << trie_type << std::endl;
    if (!new_trie){
        trie = std::unique_ptr<Trie>(new Trie());
        prepare();
    }

    int max = 0, min = 0; //disregard min and max for statistical purposes
    std::vector<std::chrono::high_resolution_clock::duration> durations ((unsigned long) iterations);
    for (int i = 0; i < iterations; ++i){
        if (new_trie) {
            trie = std::unique_ptr<Trie>(new Trie());
            prepare();
        }
        auto start = std::chrono::high_resolution_clock::now();
        test();
        auto stop = std::chrono::high_resolution_clock::now();
        durations[i] = stop - start;
        if (i > 0 && durations[max] < durations[i]){
            max = i;
        }
        if (i > 0 && durations[min] > durations[i]){
            min = i;
        }
    }

    std::cout << "Results: ";

    uint64_t sum = 0;
    for (int i = 0; i < iterations; ++i){
        std::cout << durations[i].count() << ", ";
        if ((i != max && i != min) || iterations <= 5){
            sum += durations[i].count();
        }
    }

    std::cout << std::endl;
    uint64_t average = sum / (iterations - (iterations > 5 ? 2ull : 0ull));
    std::cout << "Average: " << average << "ns" << std::endl;
#ifdef SIMULATE_HASH_TABLE
    std::cout << "Lookups: " << trie->get_lookups() << ", Updates: " << trie->get_updates() << std::endl;
    std::cout << "& " << average << " & " << trie->get_lookups() << " & " << trie->get_updates() << std::endl;
#endif
    std::cout << "----------------" << std::endl;
}








