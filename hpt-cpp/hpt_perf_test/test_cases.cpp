#include "test_cases.h"

void InsertTest::init() {
    rand_engine().seed(0);
    keys = BinaryKey::random_keys(key_cnt, min_len, max_len);
}
void InsertTest::test() {
    for (const auto & key : keys){
        trie->insert_key(key);
    }
}

InsertTest::InsertTest(size_t cnt, size_t min, size_t max)
        : BinaryKeyTriePerformanceTest(std::string("Insert ") + std::to_string(cnt) + ", length " +
                                       std::to_string(min) + " to " + std::to_string(max), true),
          key_cnt(cnt), max_len(max), min_len(min) {}

void PrefixSearchTest::init() {
    rand_engine().seed(0);
    keys = BinaryKey::random_keys(key_cnt, min_len, max_len);
    search_keys = BinaryKey::random_keys(search_key_cnt, search_min_len, search_max_len);
}

void PrefixSearchTest::test() {
    trie->reset_counter();
    for (const auto & key : search_keys){
        trie->prefix_search(key);
    }
}

PrefixSearchTest::PrefixSearchTest(size_t cnt, size_t min, size_t max, size_t search_cnt, size_t min_search,
                                   size_t max_search)
    : BinaryKeyTriePerformanceTest(std::string("Search ") + std::to_string(search_cnt) + ", length " +
                                           std::to_string(min_search) + " to " + std::to_string(max_search) +
                                           " (" + std::to_string(cnt) + " keys, length " +
                                           std::to_string(min) + " to " + std::to_string(max) + ")", false),
      key_cnt(cnt), max_len(max), min_len(min), search_key_cnt(search_cnt), search_max_len(max_search),
      search_min_len(min_search){}

void PrefixSearchTest::prepare() {
    for (const auto & key : keys){
        trie->insert_key(key);
    }
}


DeleteTest::DeleteTest(size_t cnt, size_t min, size_t max, size_t delete_cnt)
    : BinaryKeyTriePerformanceTest(std::string("Delete " + std::to_string(delete_cnt) +
                                   " (" + std::to_string(cnt) + " keys, length " +
                                   std::to_string(min) + " to " + std::to_string(max) + ")"), true),
    key_cnt(cnt), min_len(min), max_len(max), delete_cnt(delete_cnt) {}

void DeleteTest::init() {
    rand_engine().seed(0);
    keys = BinaryKey::random_keys(key_cnt, min_len, max_len);
}

void DeleteTest::test() {
    for (size_t i = 0; i < delete_cnt; ++i){
        trie->delete_key(keys[i]);
    }
}

void DeleteTest::prepare() {
    for (const auto & key : keys){
        trie->insert_key(key);
    }
    trie->reset_counter();
}







