#ifndef HPT_CPP_BINARYKEYTRIEPERFORMANCETEST_H
#define HPT_CPP_BINARYKEYTRIEPERFORMANCETEST_H

#include "BinaryKeyTrie.h"

class BinaryKeyTriePerformanceTest {
public:
    BinaryKeyTriePerformanceTest(std::string name, bool new_trie);
    void run(int iterations);
protected:
    std::unique_ptr<BinaryKeyTrie> trie;
    virtual void test() = 0;
    virtual void init() = 0;
    virtual void prepare() {};
private:
    template <class Trie>
    void run_test(const std::string& trie_type, int iterations);
    const std::string name;
    const bool new_trie;
};


#endif //HPT_CPP_BINARYKEYTRIEPERFORMANCETEST_H
