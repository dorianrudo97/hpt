#include <iostream>

#include "test_cases.h"

int main(int argc, char **argv) {
    std::cout << "*** Insert, increase number of keys ***" << std::endl;
    InsertTest (1000, 200, 200).run(10);
    InsertTest (10000, 200, 200).run(10);
    InsertTest (100000, 200, 200).run(10);
    InsertTest (1000000, 200, 200).run(1);

    std::cout << "*** Insert, increase length of keys ***" << std::endl;
    InsertTest (1000, 1000, 1000).run(10);
    InsertTest (1000, 10000, 10000).run(10);
    InsertTest (1000, 100000, 100000).run(10);
    InsertTest (1000, 1000000, 1000000).run(10);

    std::cout << "*** Prefix search, increase number of keys ***" << std::endl;
    PrefixSearchTest (1000, 1000, 1000, 1000, 500, 1000).run(10);
    PrefixSearchTest (10000, 1000, 1000, 1000, 500, 1000).run(10);
    PrefixSearchTest (100000, 1000, 1000, 1000, 500, 1000).run(10);
    PrefixSearchTest (1000000, 1000, 1000, 1000, 500, 1000).run(1);

    std::cout << "*** Prefix search, increase length of keys ***" << std::endl;
    PrefixSearchTest (1000, 1000, 1000, 1000, 1000, 1000).run(10);
    PrefixSearchTest (1000, 10000, 10000, 1000, 10000, 10000).run(10);
    PrefixSearchTest (1000, 100000, 100000, 1000, 100000, 100000).run(10);
    PrefixSearchTest (1000, 1000000, 1000000, 1000, 1000000, 1000000).run(1);

    std::cout << "*** additional tests ***" << std::endl;
    PrefixSearchTest (10000000, 64, 64, 1000, 64, 64).run(1);
    InsertTest (10000000, 64, 64).run(1);

    std::cout << "*** Delete keys ***" << std::endl;
    DeleteTest (1000, 10000, 10000, 500).run(10);
}