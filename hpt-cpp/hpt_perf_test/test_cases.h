#ifndef HPT_CPP_TEST_CASES_H
#define HPT_CPP_TEST_CASES_H

#include <vector>
#include "BinaryKey.h"
#include "BinaryKeyTriePerformanceTest.h"


class InsertTest : public BinaryKeyTriePerformanceTest {
public:
    InsertTest(size_t cnt, size_t min, size_t max);
protected:
    virtual void init() override;
    virtual void test() override;
private:
    std::vector<BinaryKey> keys;
    const size_t key_cnt, max_len, min_len;
};

class PrefixSearchTest : public BinaryKeyTriePerformanceTest {
public:
    PrefixSearchTest(size_t cnt, size_t min, size_t max, size_t search_cnt, size_t min_search, size_t max_search);
protected:
    virtual void init() override;
    virtual void test() override;
    virtual void prepare() override;
private:
    std::vector<BinaryKey> keys, search_keys;
    const size_t key_cnt, max_len, min_len, search_key_cnt, search_min_len, search_max_len;
};

class DeleteTest : public BinaryKeyTriePerformanceTest {
public:
    DeleteTest (size_t cnt, size_t min, size_t max, size_t delete_cnt);
protected:
    virtual void init() override;
    virtual void test() override;
    virtual void prepare() override;
private:
    std::vector<BinaryKey> keys;
    const size_t key_cnt, max_len, min_len, delete_cnt;
};

#endif //HPT_CPP_TEST_CASES_H
