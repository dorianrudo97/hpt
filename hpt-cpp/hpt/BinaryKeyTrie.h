#ifndef HPT_CPP_BINARYKEYTRIE_H
#define HPT_CPP_BINARYKEYTRIE_H


#include "BinaryKey.h"

class BinaryKeyTrie {
public:
    class AccessCounter{
    public:
        int get_updates() const { return updates; }
        int get_lookups() const { return lookups; }
        void incr_updates(int by = 1) { updates += by; }
        void incr_lookups(int by = 1) { lookups += by; }
        void reset() { updates = lookups = 0; }
    private:
        int updates = 0, lookups = 0;
    };

    BinaryKeyTrie() : counter(new AccessCounter) {};
    virtual BinaryKey prefix_search(const BinaryKey& key) const = 0;
    virtual void insert_key(const BinaryKey& key) = 0;
    virtual void delete_key(const BinaryKey& key) = 0;
    virtual std::vector<BinaryKey> traverse() const = 0;

    int get_updates() const { return counter->get_updates(); }
    int get_lookups() const { return counter->get_lookups(); }
    void reset_counter() const { counter->reset(); }

protected:
    std::unique_ptr<BinaryKeyTrie::AccessCounter> counter ;
};


#endif //HPT_CPP_BINARYKEYTRIE_H
