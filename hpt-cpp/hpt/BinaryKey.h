#ifndef HPT_CPP_BINARYKEY_H
#define HPT_CPP_BINARYKEY_H

#include <memory>
#include <string>
#include <vector>
#include <random>

std::default_random_engine& rand_engine();

class BinaryKey;

namespace std {
    template<>
    struct hash<BinaryKey> {
        size_t operator()(const BinaryKey& key) const;
    };
}

class BinaryKey {
    friend std::hash<BinaryKey>;
public:
    BinaryKey(const BinaryKey& key);
    BinaryKey();
    static BinaryKey make(const std::vector<uint64_t>& data, size_t len);
    static BinaryKey make(uint64_t data, size_t len);
    BinaryKey prefix(size_t length) const;
    BinaryKey slice(size_t pos, size_t len) const;
    BinaryKey operator+(const BinaryKey& other) const;
    bool operator[](size_t index) const;
    bool operator==(const BinaryKey& other) const;
    bool operator!=(const BinaryKey& other) const;
    bool operator<(const BinaryKey& other) const;
    size_t first_different_bit(const BinaryKey& other) const;
    size_t length() const;
    bool is_prefix_of(const BinaryKey &other) const;
    std::string str() const;
    static BinaryKey random(size_t length);
    static std::vector<BinaryKey> random_keys(size_t count, size_t min_len, size_t max_len);
private:
    BinaryKey(std::vector<uint64_t>& data, size_t len);
    std::shared_ptr<std::vector<uint64_t>> data; //cannot be const because stl containers don't like that
    size_t len;
};


#endif //HPT_CPP_BINARYKEY_H
