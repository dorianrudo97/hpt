#ifndef HPT_CPP_NODE_H
#define HPT_CPP_NODE_H

#include "BinaryKey.h"

template <typename T>
struct GeneralNode : public std::enable_shared_from_this<GeneralNode<T>> {
    GeneralNode(BinaryKey key, bool is_key = false) : key(key), is_key(is_key){};

    std::shared_ptr<T> get_child(bool i) const {
        return this->children[i];
    }

    std::shared_ptr<T> get_child(const BinaryKey& key) const{
        return this->get_child(key[this->key.length()]);
    }

    void set_child(std::shared_ptr<T> node){
        node->parent = std::static_pointer_cast<T>(this->shared_from_this());
        this->children[node->key[this->key.length()]] = node;
    }

    void del_child(bool i){
        this->children[i] = nullptr;
    }

    void del_child(const BinaryKey& key){
        del_child(key[this->key.length()]);
    }

    bool has_child(bool i) const{
        return this->children[i].operator bool();
    }

    bool has_child(const BinaryKey& key) const{
        return this->has_child(key[this->key.length()]);
    }

    bool is_leaf() const {
        return !children[0] && !children[1];
    }

    std::shared_ptr<T> any_child(){
        return has_child(0) ? get_child(0) : get_child(1);
    }

    std::shared_ptr<T> children [2];
    std::weak_ptr<T> parent;
    const BinaryKey key;
    bool is_key;

    static void traverse_in_order(std::vector<BinaryKey>& keys, const std::shared_ptr<T> node){
        if (node->is_key){
            keys.push_back(node->key);
        }
        if (node->has_child(0)){
            traverse_in_order(keys, node->get_child(0));
        }
        if (node->has_child(1)){
            traverse_in_order(keys, node->get_child(1));
        }
    }
};

#endif //HPT_CPP_NODE_H
