//
// Created by Dorian on 08.05.2016.
//

#ifndef HPT_CPP_PATRICIATRIEFAST_H
#define HPT_CPP_PATRICIATRIEFAST_H

#include <memory>
#include "BinaryKey.h"
#include "BinaryKeyTrie.h"
#include "Node.h"

class PatriciaTrie : public BinaryKeyTrie {
public:
    struct Node : public GeneralNode<Node> {
        Node(const BinaryKey &key, bool is_key = false) : GeneralNode(key, is_key) { }
    };
    PatriciaTrie();
    virtual BinaryKey prefix_search(const BinaryKey& key) const override ;
    void insert_key(const BinaryKey& key) override ;
    void delete_key(const BinaryKey& key) override ;
    std::vector<BinaryKey> traverse() const override;
private:
    std::shared_ptr<Node> get_node(const BinaryKey& key) const;
    std::shared_ptr<Node> root;
};


#endif //HPT_CPP_PATRICIATRIEFAST_H
