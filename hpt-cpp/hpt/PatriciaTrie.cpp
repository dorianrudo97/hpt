#include <iostream>
#include "PatriciaTrie.h"

#ifdef SIMULATE_HASH_TABLE
#define INCR_UPDATES(...) counter->incr_updates(__VA_ARGS__)
#define INCR_LOOKUPS(...) counter->incr_lookups(__VA_ARGS__)
#else
#define INCR_UPDATES(...)
#define INCR_LOOKUPS(...)
#endif

PatriciaTrie::PatriciaTrie() : root(std::make_shared<Node>(BinaryKey())) { }

BinaryKey PatriciaTrie::prefix_search(const BinaryKey &key) const {
    std::shared_ptr<Node> cur_node = get_node(key);
    while (!cur_node->is_key){
        cur_node = cur_node->has_child(0) ? cur_node->get_child(0) : cur_node->get_child(1);
        INCR_LOOKUPS();
    }
    return cur_node->key;
}

void PatriciaTrie::delete_key(const BinaryKey &key) {
    std::shared_ptr<Node> to_del = get_node(key);
    if (to_del->has_child(0) && to_del->has_child(1)){
        to_del->is_key = false;
    } else {
        std::shared_ptr<Node> parent = to_del->parent.lock();
        INCR_LOOKUPS(1);
        if (to_del->has_child(0) || to_del->has_child(1)){
            parent->set_child(to_del->has_child(0) ? to_del->get_child(0) : to_del->get_child(1));
            INCR_LOOKUPS();
            INCR_UPDATES(3);
        } else if (parent->key.length() == 0 || parent->is_key){
            parent->del_child(key);
            INCR_UPDATES(2);
        } else {
            std::shared_ptr<Node> sibling = parent->get_child(!key[parent->key.length()]);
            std::shared_ptr<Node> grandparent = parent->parent.lock();
            INCR_LOOKUPS(2);
            grandparent->set_child(sibling);
            INCR_UPDATES(3);
        }
    }
}

void PatriciaTrie::insert_key(const BinaryKey& key) {
    std::shared_ptr<Node> new_node = std::make_shared<Node>(key, true);
    std::shared_ptr<Node> cur_node = get_node(key);
    if (cur_node->key.is_prefix_of(key)){
        if (cur_node->key.length() == key.length()){
            cur_node->is_key = true;
        } else {
            cur_node->set_child(new_node);
            INCR_UPDATES(2);
        }
    } else if (key.is_prefix_of(cur_node->key)) {
        cur_node->parent.lock()->set_child(new_node);
        new_node->set_child(cur_node);
        INCR_LOOKUPS();
        INCR_UPDATES(3);
    } else {
        size_t common_pref_len = cur_node->key.first_different_bit(key) - 1;
        std::shared_ptr<Node> inter = std::make_shared<Node>(key.prefix(common_pref_len));
        std::shared_ptr<Node> parent = cur_node->parent.lock();
        parent->set_child(inter);
        inter->set_child(new_node);
        inter->set_child(cur_node);
        INCR_LOOKUPS(1);
        INCR_UPDATES(4);
    }
}

std::shared_ptr<PatriciaTrie::Node> PatriciaTrie::get_node(const BinaryKey& key) const {
    std::shared_ptr<Node> cur_node = this->root;
    INCR_LOOKUPS();
    while (cur_node->key.length() < key.length()
           && cur_node->key.is_prefix_of(key) && cur_node->has_child(key)){
        cur_node = cur_node->get_child(key);
        INCR_LOOKUPS();
    }
    return cur_node;
}

std::vector<BinaryKey> PatriciaTrie::traverse() const {
    std::vector<BinaryKey> keys;
    Node::traverse_in_order(keys, this->root);
    return keys;
}
