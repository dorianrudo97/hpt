#include <iostream>
#include "HashedPatriciaTrie.h"

#ifdef SIMULATE_HASH_TABLE
#define INCR_UPDATES(...) counter->incr_updates(__VA_ARGS__)
#define INCR_LOOKUPS(...) counter->incr_lookups(__VA_ARGS__)
#else
#define INCR_UPDATES(...)
#define INCR_LOOKUPS(...)
#endif

#define LOG2(X) ((size_t)(8 * sizeof(unsigned long long) - __builtin_clzll((X)) - 1))

#define MSD(A, B) ((size_t)(8 * sizeof(unsigned long long) - __builtin_clzll(A ^ B) - 1))
#define MSD_LEN(A, B) (B & (~0u << MSD(A, B)))

HashedPatriciaTrie::HashedPatriciaTrie() : root(std::make_shared<Node>(BinaryKey())) {
    key_map[root->key] = root;
}

BinaryKey HashedPatriciaTrie::prefix_search(const BinaryKey &key) const {
    std::shared_ptr<Node> node = get_node(key);
    if (node->is_key){
        return node->key;
    }
    return node->get_key2();
}

void HashedPatriciaTrie::insert_key(const BinaryKey &key) {
    std::shared_ptr<Node> node = get_node(key);
    if ((node->is_key && node->key == key) || (node->has_key2() && node->get_key2() == key)){
        return;
    }
    std::shared_ptr<Node> parent = node->parent.lock();
    INCR_LOOKUPS();
    std::shared_ptr<Node> new_node = std::make_shared<Node>(key, true);
    INCR_UPDATES();
    if (key.is_prefix_of(node->key)){
        if (key.length() == parent->key.length()){
            parent->is_key = true;
            return;
        } else if (key.length() == node->key.length()){
            node->is_key = true;
            return;
        } else { //the new node will be between node and its parent
            parent->set_child(new_node);
            new_node->set_child(node);
            INCR_UPDATES();
            update_msd(parent, new_node, node);
        }
    } else if (node->key.is_prefix_of(key)){
        if (node->is_leaf() && node != root){
            node->key2_node_of.lock()->set_key2_node(new_node);
            INCR_LOOKUPS();
            node->key2_node_of.reset();
            INCR_UPDATES();
        } else if (!node->has_key2()){
            node->set_key2_node(new_node);
        }
        node->set_child(new_node);
        INCR_UPDATES();
    } else {
        // this is if(parent->has_child(key)) but !parent->has_child(key) should never occur
        // because of the adapted search which does not traverse the tree farther down than 1 at the end
        BinaryKey common_prefix = key.prefix(node->key.first_different_bit(key) - 1);
        std::shared_ptr<Node> new_parent = std::make_shared<Node>(common_prefix);
        key_map[common_prefix] = new_parent;
        new_parent->set_key2_node(new_node);
        parent->set_child(new_parent);
        new_parent->set_child(node);
        new_parent->set_child(new_node);
        INCR_UPDATES(3);
        update_msd(parent, new_parent, node);
        add_msd(new_parent, new_node);
    }
    key_map[new_node->key] = new_node;
}

void HashedPatriciaTrie::delete_key(const BinaryKey &key) {
    std::shared_ptr<Node> node = read_node(key);
    INCR_UPDATES();
    if (node->has_key2()){
        node->is_key = false;
        return;
    }
    std::shared_ptr<Node> parent = node->parent.lock();
    std::shared_ptr<Node> child = node->any_child();
    INCR_LOOKUPS(2);
    key_map.erase(key);
    delete_msd(parent, node);
    if (child){
        delete_msd(node, child);
        parent->set_child(child);
        add_msd(parent, child);
        INCR_UPDATES();
    } else {
        parent->del_child(key);
        INCR_UPDATES();
        std::shared_ptr<Node> referencing = node->key2_node_of.lock();
        if (!referencing){
            if (parent->has_key2()){
                //parent is no longer needed in the trie, so it should no longer be referenced
                parent->key2_node.lock()->key2_node_of.reset();
                parent->key2_node.reset();
            }
        } else if (!parent->has_key2()){
            referencing->set_key2_node(parent);
            INCR_LOOKUPS();
            INCR_UPDATES();
        } else {
            referencing->set_key2_node(parent->key2_node.lock());
            parent->key2_node.reset();
            INCR_LOOKUPS();
            INCR_UPDATES(2);
        }

        if (!parent->is_key && parent != root){
            std::shared_ptr<Node> sibling = parent->any_child();
            std::shared_ptr<Node> grandparent = parent->parent.lock();
            INCR_LOOKUPS(2);
            delete_msd(grandparent, parent);
            key_map.erase(parent->key);
            grandparent->set_child(sibling);
            INCR_UPDATES(2);
            add_msd(grandparent, sibling);
        }
    }
}

std::vector<BinaryKey> HashedPatriciaTrie::traverse() const {
    std::vector<BinaryKey> keys;
    Node::traverse_in_order(keys, this->root);
    return keys;
}

std::shared_ptr<HashedPatriciaTrie::Node> HashedPatriciaTrie::get_node(const BinaryKey &key) const {
    std::shared_ptr<Node> node = read_node(key);
    if (node && (node->key == key || (node->has_key2() && node->get_key2() == key))) {
        return node;
    }
    if (!root->has_child(key)){
        return root;
    }

    size_t bin_search_m = 1u << LOG2(key.length());
    size_t cur_len = 0;
    node = root;

    while (bin_search_m > 0){
        BinaryKey pref = key.prefix(cur_len + bin_search_m);
        std::shared_ptr<Node> found = read_node(pref);
        if (found){
            node = found;
            cur_len += bin_search_m;
            if (cur_len >= key.length() || !node->has_child(key)){
                break;
            }
        } else if (pref.is_prefix_of(node->get_child(key)->key)){
            cur_len += bin_search_m;
        }

        bin_search_m >>= 1;
    }

    if (node->key.is_prefix_of(key) && node->key.length() < key.length() && node->has_child(key)){
        INCR_LOOKUPS();
        return node->get_child(key);
    }

    return node;
}

std::shared_ptr<HashedPatriciaTrie::Node> HashedPatriciaTrie::read_node(const BinaryKey &key) const {
    INCR_LOOKUPS();
    auto node_in_map = key_map.find(key);
    if (node_in_map != key_map.end()){
        return node_in_map->second;
    }
    return nullptr;
}

void HashedPatriciaTrie::add_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& child) {
    size_t len = MSD_LEN(parent->key.length(), child->key.length());
    if (len > parent->key.length() && len < child->key.length()){
        INCR_UPDATES();
        key_map[child->key.prefix(len)] = child;
    }
}

void HashedPatriciaTrie::update_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& new_node,
                                        const std::shared_ptr<Node>& child) {
    size_t old_msd = MSD_LEN(parent->key.length(), child->key.length());
    if (old_msd > new_node->key.length()){ //msd already between child and new_node
        add_msd(parent, new_node);
    } else if (old_msd < new_node->key.length()){ //msd already between parent and new_node
        key_map[new_node->key.prefix(old_msd)] = new_node; //update msd
        INCR_UPDATES();
        add_msd(new_node, child);
    } else {
        add_msd(parent, new_node);
        add_msd(new_node, child);
    }
}

void HashedPatriciaTrie::delete_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& child) {
    size_t old_msd = MSD_LEN(parent->key.length(), child->key.length());
    if (old_msd != parent->key.length() && old_msd != child->key.length()){
        key_map.erase(child->key.prefix(old_msd));
        INCR_UPDATES();
    }
}


bool HashedPatriciaTrie::Node::has_key2() const {
    return !key2_node.expired();
}

const BinaryKey& HashedPatriciaTrie::Node::get_key2() const {
    return key2_node.lock()->key;
}

void HashedPatriciaTrie::Node::set_key2_node(std::shared_ptr<Node> node) {
    key2_node = node;
    node->key2_node_of = std::static_pointer_cast<Node>(this->shared_from_this());
}

bool HashedPatriciaTrie::Node::is_key2() const {
    return !key2_node_of.expired();
}







