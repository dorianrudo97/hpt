#include "BinaryKey.h"
#include <iostream>

const size_t BITS = (sizeof(uint64_t) * 8);

inline size_t data_len(size_t len){
    return (len + BITS - 1) / BITS;
}

inline size_t data_idx(size_t idx){
    return idx / BITS;
}

BinaryKey BinaryKey::make(const std::vector<uint64_t> &data, size_t len){
    const size_t d_len = data_len(len);
    std::vector<uint64_t> d (d_len);
    if (len > 0){
        std::copy(data.begin(), data.begin() + d_len, d.begin());
        d[data_idx(len - 1)] &= ~0ull >> (BITS - len % BITS);
    }
    return BinaryKey(d, len);
}

BinaryKey BinaryKey::make(uint64_t data, size_t len) {
    std::vector<uint64_t> d {data & ~0ull >> (BITS - len % BITS)};
    return BinaryKey(d, len);
}

BinaryKey::BinaryKey(std::vector<uint64_t> &data, size_t len)
        : len(len), data(std::make_shared<std::vector<uint64_t>>(data)){}

size_t BinaryKey::first_different_bit(const BinaryKey &other) const {
    const size_t min_size = data_len(std::min(this->len, other.len));
    for (size_t i = 0; i < min_size; ++i){
        const uint64_t x = (*this->data)[i] ^ (*other.data)[i];
        if (x){
            return i * BITS + __builtin_ctzll(x) + 1;
        }
    }
    return 0;
}

bool BinaryKey::is_prefix_of(const BinaryKey &other) const {
    if (this->len > other.len){
        return false;
    }
    if (this->len == 0){
        return true;
    }
    size_t i;
    for (i = 0; i < data_len(this->length()) - 1; ++i){
        if ((*this->data)[i] != (*other.data)[i]){
            return false;
        }
    }
    const uint64_t mask = ~0ull >> (BITS - this->len % BITS);
    return ((*this->data)[i] & mask) == ((*other.data)[i] & mask);
}

size_t BinaryKey::length() const {
    return this->len;
}

bool BinaryKey::operator==(const BinaryKey &other) const {
    if (this->len != other.len){
        return false;
    }
    for (size_t i = 0; i < this->data->size(); ++i){
        if ((*this->data)[i] != (*other.data)[i]){
            return false;
        }
    }
    return true;
}

BinaryKey BinaryKey::prefix(size_t length) const {
    return BinaryKey::make(*this->data, length);
}

std::string BinaryKey::str() const {
    std::string s (this->len, '0');
    for (size_t i = 0; i < this->len; ++i){
        if ((*this)[i]){
            s[i] = '1';
        }
    }
    return s;
}

bool BinaryKey::operator[](size_t index) const {
    return static_cast<bool>((*this->data)[data_idx(index)] & (1ull << (index % BITS)));
}

BinaryKey BinaryKey::operator+(const BinaryKey &other) const {
    size_t key_len = this->len + other.len;
    std::vector<uint64_t> key_data (data_len(key_len));
    std::copy(this->data->begin(), this->data->end(), key_data.begin());
    size_t offset = this->len % BITS;
    size_t data_size = this->data->size();
    for (size_t i = 0; i < other.data->size(); ++i){
        if (offset > 0) {
            key_data[i + data_size - 1] |= (*other.data)[i] << offset;
            key_data[i + data_size] = (*other.data)[i] >> (BITS - offset);
        } else {
            key_data[i + data_size] = (*other.data)[i];
        }
    }
    return BinaryKey (key_data, key_len);
}

BinaryKey BinaryKey::slice(size_t pos, size_t len) const {
    size_t data_from = data_idx(pos), data_to = data_idx(pos + len);
    std::vector<uint64_t> key_data (data_to - data_from + 1);
    std::copy(this->data->begin() + data_from, this->data->begin() + (data_to + 1), key_data.begin());
    size_t offset = pos % BITS;
    for (int i = 0; i < key_data.size(); ++i){
        if (i > 0){
            key_data[i - 1] |= key_data[i] << (BITS - offset);
        }
        key_data[i] >>= offset;
    }
    return BinaryKey(key_data, len);
}

BinaryKey BinaryKey::random(size_t length) {
    size_t l = data_len(length);
    std::vector<uint64_t> data (l);
    std::uniform_int_distribution<uint64_t> distribution(0, std::numeric_limits<uint64_t>::max());
    for (size_t i = 0; i < l; ++i){
        data[i] = distribution(rand_engine());
    }
    return BinaryKey::make(data, length);
}

bool BinaryKey::operator!=(const BinaryKey &other) const {
    return !this->operator==(other);
}

template <typename T, T m, int k>
static inline T swapbits(T p) {
    T q = ((p>>k)^p)&m;
    return p^q^(q<<k);
}

uint64_t reverse_64_bit (uint64_t n) {
    static const uint64_t m0 = 0x5555555555555555LLU;
    static const uint64_t m1 = 0x0300c0303030c303LLU;
    static const uint64_t m2 = 0x00c0300c03f0003fLLU;
    static const uint64_t m3 = 0x00000ffc00003fffLLU;
    n = ((n>>1)&m0) | (n&m0)<<1;
    n = swapbits<uint64_t, m1, 4>(n);
    n = swapbits<uint64_t, m2, 8>(n);
    n = swapbits<uint64_t, m3, 20>(n);
    n = (n >> 34) | (n << 30);
    return n;
}

bool BinaryKey::operator<(const BinaryKey &other) const {
    size_t min_len = std::min(this->data->size(), other.data->size());
    for (size_t i = 0; i < min_len; ++i){
        uint64_t this_data = reverse_64_bit((*this->data)[i]);
        uint64_t other_data = reverse_64_bit((*other.data)[i]);
        if (this_data != other_data){
            return this_data < other_data;
        }
    }
    return this->len < other.len;
}


std::default_random_engine& rand_engine(){
    static std::default_random_engine engine{0};
    return engine;
}

namespace std {
    size_t hash<BinaryKey>::operator()(const BinaryKey& key) const {
        size_t hash = key.len;
        for (uint64_t const& x : *key.data){
            if (sizeof(uint64_t) == sizeof(size_t)){
                hash ^= x;
            } else {
                hash ^= static_cast<size_t> (x ^ (x >> 32));
            }
            hash = (hash << 3) | (hash >> (sizeof(size_t) - 3));
        }
        return hash;
    }
}

std::vector<BinaryKey> BinaryKey::random_keys(size_t count, size_t min_len, size_t max_len) {
    std::vector<BinaryKey> keys;
    std::uniform_int_distribution<size_t> distribution (min_len, max_len);
    while (keys.size() < count){
        keys.push_back(BinaryKey::random(distribution(rand_engine())));
    }
    return keys;
}

BinaryKey::BinaryKey(const BinaryKey &key) : data(key.data), len(key.len) {}

BinaryKey::BinaryKey() : len(0), data(std::make_shared<std::vector<uint64_t>>()){ }



