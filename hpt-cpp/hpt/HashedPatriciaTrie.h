#ifndef HPT_CPP_HASHEDPATRICIATRIEFAST_H
#define HPT_CPP_HASHEDPATRICIATRIEFAST_H


#include "BinaryKey.h"
#include "Node.h"
#include "BinaryKeyTrie.h"
#include <unordered_map>

class HashedPatriciaTrie : public BinaryKeyTrie {
public:
    struct Node : public GeneralNode<Node> {
        Node(const BinaryKey &key, bool is_key = false) : GeneralNode(key, is_key) { }
        std::weak_ptr<Node> key2_node, key2_node_of;
        bool has_key2() const;
        bool is_key2() const;
        const BinaryKey& get_key2() const;
        void set_key2_node(std::shared_ptr<Node> node);
    };
    HashedPatriciaTrie();
    virtual BinaryKey prefix_search(const BinaryKey& key) const override ;
    void insert_key(const BinaryKey& key) override ;
    void delete_key(const BinaryKey& key) override ;
    std::vector<BinaryKey> traverse() const override;
private:
    std::shared_ptr<Node> get_node(const BinaryKey& key) const;
    std::shared_ptr<Node> root;
    std::unordered_map<BinaryKey, std::shared_ptr<Node>> key_map;
    std::shared_ptr<Node> read_node(const BinaryKey& key) const;
    void add_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& child);
    void update_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& new_node,
                    const std::shared_ptr<Node>& child);
    void delete_msd(const std::shared_ptr<Node>& parent, const std::shared_ptr<Node>& child);
};


#endif //HPT_CPP_HASHEDPATRICIATRIEFAST_H
