#include <PatriciaTrie.h>
#include "gtest/gtest.h"
#include "BinaryKey.h"
#include <algorithm>
#include <set>
#include <unordered_set>
#include <HashedPatriciaTrie.h>

namespace {
    bool test_traverse(std::vector<BinaryKey> &traverse, std::vector<BinaryKey> &keys){
        std::set<BinaryKey> key_set (keys.begin(), keys.end());
        size_t i = 0;
        for (const BinaryKey& key : key_set){
            if (i >= traverse.size() || traverse[i] != key){

                //Print keys to help find the bug
                std::cout << "------------------actual:-----------------" << std::endl;
                for (const BinaryKey& k : traverse){
                    std::cout << k.str() << std::endl;
                }
                std::cout << "-----------------expected:----------------" << std::endl;
                for (const BinaryKey& k : key_set){
                    std::cout << k.str() << std::endl;
                }
                std::cout << traverse.size() << " " << key_set.size() << std::endl;

                return false;
            }
            ++i;
        }
        return true;
    }

    std::unordered_set<BinaryKey> prefix_search(const std::vector<BinaryKey> &keys, const BinaryKey& x){
        size_t longest = 0;
        std::unordered_set<BinaryKey> prefix_matches;
        for(const BinaryKey& key : keys){
            size_t i = 0;
            while (i < key.length() && i < x.length() && key[i] == x[i]){
                ++i;
            }
            if (i == longest){
                prefix_matches.insert(key);
            } else if (i > longest){
                longest = i;
                prefix_matches = {key};
            }
        }
        return prefix_matches;
    }
}

template <typename T>
class TrieTest : public ::testing::Test {
public:
    T trie;
protected:
    virtual void SetUp() override {
        rand_engine().seed(0);
    }
};

typedef ::testing::Types<HashedPatriciaTrie, PatriciaTrie> TrieTypes;

TYPED_TEST_CASE(TrieTest, TrieTypes);

#define insert_random_keys(NUM, MIN_LEN, MAX_LEN) \
    std::vector<BinaryKey> keys = BinaryKey::random_keys(NUM, MIN_LEN, MAX_LEN); \
    for (const BinaryKey &key : keys){ \
        this->trie.insert_key(key); \
    } \

TYPED_TEST(TrieTest, insert_10) {
    insert_random_keys(10, 8, 8);
    std::vector<BinaryKey> traverse = this->trie.traverse();
    EXPECT_TRUE(test_traverse(traverse, keys));
}

TYPED_TEST(TrieTest, insert_1000) {
    insert_random_keys(1000, 8, 200);
    std::vector<BinaryKey> traverse = this->trie.traverse();
    EXPECT_TRUE(test_traverse(traverse, keys));
}

TYPED_TEST(TrieTest, prefix_search_10) {
    insert_random_keys(10, 8, 200);
    std::vector<BinaryKey> search_keys = BinaryKey::random_keys(10, 4, 200);
    for(const BinaryKey& key : search_keys){
        std::unordered_set<BinaryKey> matches = prefix_search(keys, key);
        BinaryKey result = this->trie.prefix_search(key);
        EXPECT_EQ(1, matches.count(result));
    }
}

TYPED_TEST(TrieTest, prefix_search_1000) {
    insert_random_keys(1000, 5, 250);
    std::vector<BinaryKey> search_keys = BinaryKey::random_keys(55, 5, 250);
    for(const BinaryKey& key : search_keys){
        std::unordered_set<BinaryKey> matches = prefix_search(keys, key);
        BinaryKey result = this->trie.prefix_search(key);
        EXPECT_EQ(1, matches.count(result));
    }
}

TYPED_TEST(TrieTest, delete_10) {
    insert_random_keys(10, 5, 50);
    std::vector<BinaryKey> remaining;
    std::unordered_set<BinaryKey> deleted;
    std::uniform_int_distribution<size_t> distribution (0, keys.size()-1);
    while (deleted.size() < 5){
        size_t i = distribution(rand_engine());
        if(deleted.count(keys[i]) == 0){
            deleted.insert(keys[i]);
            this->trie.delete_key(keys[i]);
        }
    }
    for (const BinaryKey& key : keys){
        if (deleted.count(key) == 0){
            remaining.push_back(key);
        }
    }
    std::vector<BinaryKey> traverse = this->trie.traverse();
    EXPECT_TRUE(test_traverse(traverse, remaining));
}

TYPED_TEST(TrieTest, delete_1000) {
    insert_random_keys(1000, 10, 250);
    std::vector<BinaryKey> remaining;
    std::unordered_set<BinaryKey> deleted;
    std::uniform_int_distribution<size_t> distribution (0, keys.size()-1);
    while (deleted.size() < keys.size() / 2){
        size_t i = distribution(rand_engine());
        if(deleted.count(keys[i]) == 0){
            deleted.insert(keys[i]);
            this->trie.delete_key(keys[i]);
        }
    }
    for (const BinaryKey& key : keys){
        if (deleted.count(key) == 0){
            remaining.push_back(key);
        }
    }
    std::vector<BinaryKey> traverse = this->trie.traverse();
    EXPECT_TRUE(test_traverse(traverse, remaining));
}