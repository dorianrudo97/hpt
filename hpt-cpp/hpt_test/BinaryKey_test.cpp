#include "gtest/gtest.h"
#include "BinaryKey.h"
#include <algorithm>

TEST(BinaryKey, test_str_1) {
    BinaryKey key = BinaryKey::make(12, 4);
    EXPECT_EQ("0011", key.str());
}

TEST(BinaryKey, test_str_2) {
    BinaryKey key = BinaryKey::make(0b11010010110, 11);
    EXPECT_EQ("01101001011", key.str());
}

TEST(BinaryKey, test_str_3) {
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    EXPECT_EQ("010110010010001100001110110011000111111001011010001010001001000101100011001011000010001011100100010110011001010010011011001010001001001110001110001011010100000101001000111000010101010010100100", key.str());
}

TEST(BinaryKey, test_str_4) {
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3-10);
    EXPECT_EQ("01011001001000110000111011001100011111100101101000101000100100010110001100101100001000101110010001011001100101001001101100101000100100111000111000101101010000010100100011100001010101", key.str());
}

TEST(BinaryKey, test_prefix) {
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    BinaryKey prefix = key.prefix(65);
    EXPECT_EQ("01011001001000110000111011001100011111100101101000101000100100010", prefix.str());
}

TEST(BinaryKey, test_is_prefix) {
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    BinaryKey prefix = key.prefix(65);
    EXPECT_TRUE(prefix.is_prefix_of(key));
    EXPECT_FALSE(key.is_prefix_of(prefix));
}

TEST(BinaryKey, test_operator_plus_1) {
    std::vector<uint64_t> data1 {1235345234ull}, data2 {4564356ull};
    BinaryKey key1 = BinaryKey::make(data1, 15), key2 = BinaryKey::make(data2, 10);
    BinaryKey key = key1 + key2;
    EXPECT_EQ(key1.str() + key2.str(), key.str());
}

TEST(BinaryKey, test_operator_plus_2) {
    std::vector<uint64_t> data1 {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    std::vector<uint64_t> data2 {4534446436452463447ull, 7687543246576432468ull};
    BinaryKey key1 = BinaryKey::make(data1, 64*3-45), key2 = BinaryKey::make(data2, 64*2-10);
    BinaryKey key = key1 + key2;
    EXPECT_EQ(key1.str() + key2.str(), key.str());
}

TEST(BinaryKey, test_hash) {
    std::vector<uint64_t> data1 {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key1 = BinaryKey::make(data1, 64*3), key2 = BinaryKey::make(data1, 64*3),
            key3 = BinaryKey::make(data1, 64*3-1);
    std::hash<BinaryKey> hash;
    EXPECT_EQ(hash(key1), hash(key2));
    EXPECT_NE(hash(key1), hash(key3));
}

TEST(BinaryKey, test_subscript) {
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    std::string s = key.str();
    for (size_t i = 0; i < key.length(); ++i){
        EXPECT_EQ(s[i] == '0' ? 0 : 1, key[i]);
    }
}

TEST(BinaryKey, test_equals){
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    BinaryKey key1 = key.prefix(64*3-23), key2 = key.prefix(64*3-23);
    EXPECT_TRUE(key1 == key2);
}

TEST(BinaryKey, test_sclice){
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    BinaryKey slice = key.slice(23, 69);
    EXPECT_EQ(key.str().substr(23, 69), slice.str());
}

TEST(BinaryKey, test_smaller_prefix){
    std::vector<uint64_t> data {9877619380805747866ull, 1502277692765254854ull, 2678101441997205961ull};
    BinaryKey key = BinaryKey::make(data, 64*3);
    BinaryKey key2 = key.prefix(70);
    EXPECT_LT(key2, key);
}

TEST(BinaryKey, test_smaller_1){
    BinaryKey key1 = BinaryKey::make(0b101, 3);
    BinaryKey key2 = BinaryKey::make(0b110, 3);
    EXPECT_LT(key2, key1);
}

TEST(BinaryKey, test_smaller_2){
    BinaryKey key1 = BinaryKey::make(150, 8);
    BinaryKey key2 = BinaryKey::make(150, 11);
    EXPECT_LT(key1, key2);
    EXPECT_FALSE(key2 < key1);
}

TEST(BinaryKey, test_smaller_3){
    std::vector<uint64_t> data1 {0x6fbe19d82463679ull, 0xaa3099b0282cbb9aull, 0x7d5a9bca77eff963ull};
    std::vector<uint64_t> data2 {0x6fbe19d82463679ull, 0xaf3099b0282cbb9aull, 0x7d5a9bca77eff963ull};
    BinaryKey key1 = BinaryKey::make(data1, 64*3);
    BinaryKey key2 = BinaryKey::make(data2, 64*3-3);
    EXPECT_LT(key1, key2);
    EXPECT_FALSE(key2 < key1);
}

TEST(BinaryKey, test_random){
    rand_engine().seed(0);
    BinaryKey key1 = BinaryKey::random(123);
    BinaryKey key2 = BinaryKey::random(123);
    rand_engine().seed(0);
    BinaryKey key3 = BinaryKey::random(123);
    EXPECT_EQ(key1, key3);
    EXPECT_NE(key1, key2);
}

TEST(BinaryKey, test_first_different_bit_1){
    BinaryKey key1 = BinaryKey::make(0b101, 3);
    BinaryKey key2 = BinaryKey::make(0b110, 3);
    EXPECT_EQ(1, key1.first_different_bit(key2));
    EXPECT_EQ(1, key2.first_different_bit(key1));
}

TEST(BinaryKey, test_first_different_bit_2){
    BinaryKey key1 = BinaryKey::make(0b1010101110101, 13);
    BinaryKey key2 = BinaryKey::make(0b1110101110101, 13);
    EXPECT_EQ(12, key1.first_different_bit(key2));
    EXPECT_EQ(12, key2.first_different_bit(key1));
}

TEST(BinaryKey, test_first_different_bit_3){
    std::vector<uint64_t> data1 {0x6fbe19d82463679ull, 0xaa3099b0282cbb9aull};
    std::vector<uint64_t> data2 {0x6fbe19d82463679ull, 0xa93099b0282cbb9aull};
    BinaryKey key1 = BinaryKey::make(data1, 2*64);
    BinaryKey key2 = BinaryKey::make(data2, 2*64);
    EXPECT_EQ(121, key1.first_different_bit(key2));
    EXPECT_EQ(121, key2.first_different_bit(key1));
}

TEST(BinaryKey, test_random_keys) {
    std::vector<BinaryKey> keys = BinaryKey::random_keys(1000, 10, 200);
    std::vector<std::string> str_keys;
    for(const BinaryKey &key : keys){
        str_keys.push_back(key.str());
    }
    std::sort(str_keys.begin(), str_keys.end());
    std::sort(keys.begin(), keys.end());
    for (size_t i = 0; i < keys.size(); ++i) {
        EXPECT_EQ(str_keys[i], keys[i].str());
    }
}