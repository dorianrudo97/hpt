\documentclass[10pt, a4paper]{article}

\usepackage[english]{babel}
\usepackage[margin=2cm, nohead]{geometry}
\usepackage{parskip}
\usepackage{listings}
\usepackage{array}
\usepackage{arydshln}
\usepackage[scaled]{beramono}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{graphicx}

\title{Analysis and Implementation of the Hashed Patricia Trie}
\date{}

\begin{document}

\maketitle

\section{Improvement}

\subsection{Idea}
	
The hashed Patricia trie data structure can be simplified by leaving the msd-nodes out of the tree.
Instead, references to the would be child node of the msd-node will be inserted into the hash table with the msd-node's key.
This reduces the number of read and write operations to the hash table.

\subsection{Consequences}

The second \emph{while}-loop in the \emph{PrefixSearch} algorithm can be replaced by a single \emph{if}-statement (cf. C++ code) since after the binary search is finished, the node with the longest common prefix has to be either the child of the found node or that node itself.
It cannot be further down as any of those nodes would either have a longer key or would have been found in the binary search via the msd-keys.
Thus 1 or 2 hash table lookups can be saved.

Concerning insertion and deletion, fewer lookups are needed when obtaining the child or parent nodes as msd-nodes do not have to be traversed anymore. 
Although nodes that formerly would have been parent and child nodes of an msd-node do not reference it anymore, they still need to be updated in almost all cases due to structural changes to the trie.

The algorithms for inserting and updating can be simplified as well (compare Python and C++ implementations).
Furthermore, the last case in the insertion algorithm (If $p_ {x_{|b(u)|+1}}(u)$ does not exist) becomes obsolete.

\subsection{Implementation}

In the C++ implementation just additional pointers to the node are inserted into the hash table.
When using a real distributed hash table (DHT) this usually is not possible.
One could of course modify the DHT to allow one entry to have multiple keys.

An option that would not require any modifications would be to insert a copy of the node to which the msd-key should point to into the table.
While this would not add any complications to \emph{PrefixSearch}, each update to a node would require an additional update to the msd-key.

The arguably best option would be to insert the key of the node to which the msd-node should point to into the table. Thereby, \emph{insert} and \emph{delete} would not need any changes and \emph{search} would maximally need one additional lookup at the end if the object read last is a key and not a node.

\section{Corrections}

\begin{itemize}
	\item \emph{PrefixSearch} would need a temporary variable for $v$ so that in the end not $v=\emptyset$.
	\item In \emph{PrefixSearch} it might occur that $p_{x_{k+1}}(v)$ does not exist. In that case one could just break the loop.
	\item In 4.2 $p_ {x_{|b(u)|+1}}$ is written instead of the correct $p_ {x_{|b(u)|+1}}(u)$.
	\item The paper implies in 4.2 and 4.3 that $r(v)$ is defined for every Patricia node $v$ which would only be the case if an infinity node would exist and all keys had the same length.
\end{itemize}

\section{Python implementation}

For the Python (3.5) implementation I followed the pseudo code and the descriptions of the \emph{insert} and \emph{delete} algorithms as close as I could. 
Therefore the naming of the variables might seem quite obscure without the paper at hand.
As keys I used tuples of 0s and 1s.

If the tool \emph{GraphViz} as well as the python packages \emph{pydotplus}, \emph{nxpydot} and \emph{graphviz} are installed, images like the following can be created:

\includegraphics[width=\textwidth]{trie0.png}


\section{C++ implementation}

The C++ implementation is my second attempt at implementing the hashed Patricia trie and should be generally the more readable one, as I did not copy the variable names from the paper but have chosen (hopefully) descriptive ones. 
Also, the afore mentioned improvement is implemented.

To represent keys the \texttt{BinaryKey} class is used which uses integers to store the data as storage efficient as possible.
Tasks such as calculating hash values or comparing keys are also accelerated by using integers.

The \texttt{PatriciaTrie} and \texttt{HashedPatriciaTrie} classes use an ordinary \texttt{std::unordered\_map} instead of a DHT.
If one were to actually use a DHT, the \texttt{Node} class would need to be adapted as well as the trie classes.

\emph{Google Test} is used as unit test framework to ensure the implementation's correctness. 
After cloning of the repository call \texttt{git submodule init} and \texttt{git submodule update} to get the test framework.
The project itself is using \emph{CMake}.

\section{Comparison of the Hashed Patricia Trie to the Patricia Trie}

Performance tests using randomly generated keys (clang 7.3.0, -O3, OS X 10.11, i5 4760k):

\begin{tabular}{l || r | r || r | r | r || r | r | r}
	\multicolumn{3}{c}{} & \multicolumn{3}{c}{Patricia trie} & \multicolumn{3}{c}{hashed Patricia trie} \\
	test case & key number & key length & time in ns & reads & writes & time in ns & reads & writes \\ \hline
	insert & 1000 & 200  & 902013 & 9903 & 3996 & 7336452 & 10845 & 5273\\
	insert & 10000 & 200 & 12159088 & 131740 & 39996 & 61992630 & 108724 & 52733 \\
	insert & 100000 & 200  & 150715759 & 1650493 & 399996 & 838398304 & 1087782 & 528180 \\
	insert & 1000000 & 200 & 2091490626 & 19821774 & 3999996  & 11340310889 & 10885032 & 5267910\\ \hdashline
	insert & 1000 & 1000 & 816187 & 9833 & 3996& 5137912 & 12834 & 5282 \\
	insert & 1000 & 10000 & 810691 & 9862 & 3996  & 7449984 & 16838 & 5273\\
	insert & 1000 & 100000 & 822370 & 9857 & 3996 & 20784552 & 19838 & 5258 \\
	insert & 1000 & 1000000 & 854799 & 9840 & 3996 & 119598663 & 22815 & 5270\\ \hdashline
	search\footnotemark & 1000 & 1000 & 249499 & 10633 & 0 & 3546739 & 11834 & 0\\
	search & 10000 & 1000 & 350537 & 14000 & 0 & 4286182 & 11833 & 0 \\
	search & 100000 & 1000 & 782190 & 17241 & 0 & 6784091 & 11828 & 0\\
	search & 1000000 & 1000 & 1751779 & 20603 & 0& 12268668 & 11885 & 0\\ \hdashline
	search\footnotemark & 1000 & 1000& 256945 & 10603 & 0 & 3446004 & 11841 & 0\\
	search & 1000 & 10000 & 270818 & 10672 & 0& 5497167 & 15856 & 0\\
	search & 1000 & 100000& 337653 & 10665 & 0& 10444629 & 18857 & 0 \\
	search & 1000 & 1000000 & 416221 & 10666 & 0 & 53878125 & 21880 & 0\\ \hdashline
	search & 10000000 & 64 & 2029280 & 23936 & 0& 25562230 & 8883 & 0 \\
	insert & 10000000 & 64 & 1899920514 & 19821405 & 3999996 & 9631237709 & 9885455 & 4267307\\ \hdashline
	delete\footnotemark & 1000 & 10000& 323727 & 6971 & & 1596870 & 2999 & 4093
\end{tabular}
\footnotetext[1]{1000 searches for keys of length between 500 and 1000 are performed}
\footnotetext[2]{1000 searches for keys of same length are performed}
\footnotetext[3]{500 deletions are performed}

Now one can see that the Patricia trie always beats the hashed Patricia trie in terms of hash table writes which was to be expected as both only need constant write operations but the hashed Patricia trie uses additional keys and msd-nodes requiring more writes.

Also, in terms of execution time the hashed Patricia trie is significantly less efficient in all cases. The most probable cause therefore is the usage of a hash table and the calculation of key prefixes.
At least the creation of prefix keys could be optimized by not creating new keys but using the old keys including their data.
Nonetheless, the worst-case search complexity for hashed Patricia tries in terms of real runtime is $\Theta(n~log~n)$ as the binary search is in $\Theta(log~n)$ and each hash table lookup is in $\Theta(n)$ due to the fact that at least one comparison is needed (key length $n$).

So the interesting result is within the reads.
When increasing the number of keys, the number of reads remains equal in the hashed Patricia trie and increases in the patricia trie.
Increasing the key length in turn leads to the opposite result.

\subsection{Average depth of key nodes in a Patricia trie}

In order to mathematically confirm the previous result, I will calculate the average depth of key nodes in a Patricia trie.

Assume all keys have an equal length $l$. Let $n$ be the number of keys in the trie and $S \subseteq \{0, ..., 2^l-1\}$ the set of these keys. 
Given a key $x$ the probability that a node with key length $i$ is on the path from the root to the node of $x$ is equal to the probability that $S$ is in the set of all sets that do not contain any key starting with $x_1...x_i\overline{x_{i+1}}$ which is:
\[
1-\frac{\binom{2^l-2^{l-i-1}}{n}}{\binom{2^l}{n}}
\]
Therefore the average depth of a key node is:
\[
\sum_{i=1}^{l-1}1-\frac{\binom{2^l-2^{l-i-1}}{n}}{\binom{2^l}{n}} \approx \sum_{i=1}^{l-1}1-(1-2^{-i-1})^n
\]
The approximation is obtained by ignoring that the values in a set are distinct. Therefore it is reasonably accurate when $\frac{n}{2^l}<\epsilon<1$ for some suitable $\epsilon$.

The approximated formula makes it clear that increasing $l$ will not lead to any noticeable increase for large enough $l$.
\pagebreak
\appendix
\section{Hashed Patricia Trie C++ Code}


\lstset{%
	basicstyle=\scriptsize\ttfamily,
	breaklines=true,
	keywordstyle=\bfseries,
	language=C++,
}
\lstinputlisting{../hpt-cpp/hpt/HashedPatriciaTrie.cpp}

\end{document}
